
Приложение опросник - React Quiz

Front-end: React и Redux,
Back-end: WordPress REST API,
Styles: Scss


Данные для опроса вводятся через пункт меню в админ панели WordPress.
Для добавления пункта в меню WordPress и передачи данных используется написанный мной плагин.
Демо https://reactquiz.pavelnikitin.now.sh




Этот проект использует [Create React App](https://github.com/facebook/create-react-app).

## Доступные скрипты

В папке проекта:

### `npm start`

Запускает приложение в режиме 'development'
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`