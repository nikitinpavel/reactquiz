//quizzes are loaded
export const getQuizzesSuccess = quizzes => ({
  type: "GET_QUIZZES_SUCCESS",
  quizzes
});

//get data for quizzes
export const getQizzes = () => ({
  type: "GET_QIZZES"
});

export const startGetQizzes = url => {
  return (dispatch, getState, axios) => {
    dispatch(getQizzes());
    axios
      .get(url)
      .then(res => {
        const data = res.data;

        const quizzes = data.map(quiz => {
          return {
            id: quiz.ID,
            title: quiz.post_title,
            description: quiz.post_content,
            img: quiz.image,
            questions: quiz.questions,
            test_results: quiz.test_results
          };
        });

        dispatch(getQuizzesSuccess(quizzes));
      })
      .catch(e => console.log(e));
  };
};

//get questions for the quiz
export const getQuizData = quiz => ({
  type: "GET_QUIZ_DATA",
  quiz
});


//start quiz
export const startQuiz = (id, history) => {
  return (dispatch, getState, axios) => {
    const quizzes = getState().quizzes.quizzesData;
    //get data for selected quiz
    const quizApi = quizzes.find(item => {
      return item.id === id;
    });
    // this object will be dispatch
    const quiz = {};

    //save image data
    quiz.img = quizApi.img

    //save quiz title
    quiz.title = quizApi.title

    //make array from api questions object
    const apiQuestions = quizApi.questions;
    const questions = [];

    Object.keys(apiQuestions).forEach(key => {
      if (apiQuestions[key].question_text) {
        questions.push(apiQuestions[key]);
      }
    });
    //save array questions in quiz object
    quiz.questions = questions;

    //make array from api test results object
    const apiTestResults = quizApi.test_results;
    const testResults = [];

    Object.keys(apiTestResults).forEach(key => {
      if (apiTestResults[key]) {
        testResults.push(apiTestResults[key]);
      }
    });
    //save array test results in quiz object
    quiz.testResults = testResults;

    //dispatch  quiz
    dispatch(getQuizData(quiz));

    //save quiz in case of page reload
    sessionStorage.setItem("quizData", JSON.stringify(quiz));

    //redirect on quiz page
    history.push(`quiz/${id}`);
  };
};

export const getUserResult = result => ({
  type: "GET_USER_RESULT",
  result
});

export const startGetUserResult = (score, history) => {
  return (dispatch, getState, axios) => {
    const testResults = getState().quizzes.quiz.testResults;

    //get result accordingly user score
    const userResult = {};
    userResult.score = score;
    userResult.text = testResults.find((result, index) => {
      return index === score;
    });

    //dispatch user result in redux state
    dispatch(getUserResult(userResult));

    //redirect on TestResult component
    history.push('/result');
  };
};
