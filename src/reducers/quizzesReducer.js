const quizzesDefaultState = {
  quizzesData: [],
  loading: false,
  quiz: {},
  userResult: {
    score: 0,
    text: ''
  }
};

export function quizzesReducer(state = quizzesDefaultState, action) {
  switch (action.type) {
    case "GET_QUIZZES_SUCCESS":
      return {
        ...state,
        quizzesData: [...action.quizzes],
        loading: false
      };

    case "GET_QIZZES":
      return {
        ...state,
        loading: true
      };

    case "GET_QUIZ_DATA":
      return {
        ...state,
        quiz: {...action.quiz}
      };

    case "GET_USER_RESULT":
      return {
        ...state,
        userResult: {...action.result}
      };

    default:
      return state;
  }
}
