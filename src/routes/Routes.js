import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import QuizzesList from '../compontens/QuizzesList/QuizzesList';
import Quiz from '../compontens/Quiz/Quiz';
import TestResult from '../compontens/TestResult/TestResult';


// The Routing Component providing all the routing Configuration
const Routes = (props) => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={QuizzesList} />
        <Route path="/quiz/:id" component={Quiz} />
        <Route path="/result" component={TestResult} />
      </Switch>
    </BrowserRouter>
  )
}

export { Routes }