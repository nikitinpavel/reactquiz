import React, { Component } from "react";
import { connect } from "react-redux";

import Layout from '../Layout/Layout';
import { startQuiz } from '../../actions/quizzesActions';
import Loader from '../Loader/Loader';


class QuizzesList extends Component {


  render() {

    return (
      <Layout>
        {this.props.loading ? <Loader /> :

        <div className="quiz-list">
            <div className="container">

              {this.props.quizzes.map(quiz => {
                return (
                <div className="quiz" key={quiz.id}>
                  <h2 className="quiz__title">{quiz.title}</h2>
                  <div className="quiz__description" dangerouslySetInnerHTML={{__html: quiz.description}}></div>
                  <img className="quiz__img" src={quiz.img} alt="quiz img" />
                  <button className="quiz__app-button app-button" onClick={() => this.props.startQuiz(quiz.id, this.props.history)}>пройти тест</button>
                </div>
                );
              })}

          </div>
        </div>

      }
      </Layout>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  quizzes: state.quizzes.quizzesData,
  loading: state.quizzes.loading
});

const mapDispatchToProps = (dispatch) => ({
  startQuiz:(id, history) => dispatch(startQuiz(id, history)),
});

export default connect(mapStateToProps, mapDispatchToProps)(QuizzesList);
