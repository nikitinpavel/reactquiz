import React from "react";
import { connect } from "react-redux";
import { Redirect, Link } from "react-router-dom";

import Layout from '../Layout/Layout';
import { ReactComponent as FacebookIcon } from '../../images/icons/facebook.svg';
import { ReactComponent as VkIcon } from '../../images/icons/vk.svg';
import { ReactComponent as TwitterIcon } from '../../images/icons/twitter.svg';
import { ReactComponent as TelegramIcon } from '../../images/icons/telegram.svg';
import { ReactComponent as RefreshIcon } from '../../images/icons/refresh.svg';

function TestResult(props) {

  const { img, title, questions } = props.quiz;

  if (!props.result.score) {
    return (
      <Redirect to={{pathname: "/"}}/>
    );
  }

  else

    return (
      <Layout>
        <div className="container">
          <div className="result">
            <div className="result__content-wrap">
              <img className="result__img" src={img} alt="quiz result"/>
              <div className="result__content">
                <h2 className="result__quiz-title">{title}</h2>
                <div className="result__score" >
                <div className="result__score-digits">
                  {props.result.score}/{questions.length}
                </div>
                  <div className="result__score-text" dangerouslySetInnerHTML={{__html: props.result.text}}>
                  </div>
                </div>
              </div>
            </div>
            <ul className="result__social">
              <li className="result__social-link"><a href={`https://www.facebook.com/sharer.php?u=${window.location.href}`}><FacebookIcon width="25px" height="25px"/></a></li>
              <li className="result__social-link"><a href={`http://vk.com/share.php?url=${window.location.href}&title=${title}`}><VkIcon width="25px" height="25px"/></a></li>
              <li className="result__social-link"><a href={`https://twitter.com/intent/tweet?url=${window.location.href}&text=${title}`}><TwitterIcon width="25px" height="25px"/></a></li>
              <li className="result__social-link"><a href={`https://telegram.me/share/url?url=${window.location.href}&text=${title}`}><TelegramIcon width="25px" height="25px"/></a></li>
            </ul>
            <Link className="result__repeat app-link" to="/"><RefreshIcon width="20px" height="20px"/>Играть еще раз</Link>
          </div>
        </div>
      </Layout>
    );
}

const mapStateToProps = (state, ownProps) => ({
  result: state.quizzes.userResult,
  quiz: state.quizzes.quiz

});

export default connect(
  mapStateToProps,
  null
)(TestResult);
