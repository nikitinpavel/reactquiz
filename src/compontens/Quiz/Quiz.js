import React, { Component } from "react";
import { connect } from "react-redux";
import { CSSTransition, TransitionGroup } from "react-transition-group";

import { startGetUserResult } from "../../actions/quizzesActions";

import Layout from "../Layout/Layout";
import Question from "../Question/Question";

class Quiz extends Component {
  constructor(props) {
    super(props);

    this.state = {
      questions: this.props.questions,
      answer: "",
      questionIndex: 0,
      score: 0
    };

    this.getAnswer = this.getAnswer.bind(this);
    this.getNext = this.getNext.bind(this);
  }

  //set user answer
  getAnswer(e) {
    let userAnswer = e.target.value;
    let question = this.state.questions[this.state.questionIndex];
    let rightAnswer = question.answer;
    //if answer is right increase score
    if (userAnswer === rightAnswer) {
      this.setState({
        score: this.state.score + 1
      });
    }
    this.setState({
      answer: userAnswer
    });
  }

  //move user to next question or get result if the test is over
  getNext() {
    let history = this.props.history;

    if (this.state.questions.length === this.state.questionIndex + 1) {
      this.props.getResult(this.state.score, history);
    } else {
      this.setState({
        questionIndex: this.state.questionIndex + 1,
        answer: ""
      });
    }
  }

  render() {
    const { questions, questionIndex } = this.state;
    let question = questions[questionIndex];

    return (
      <Layout>
        <div className="container quiz-wrap">
          <TransitionGroup>
            <CSSTransition
              timeout={500}
              classNames="slide"
              key={questionIndex}
            >
              <Question
                questionNumber={questionIndex + 1}
                questionsCount={questions.length}
                question={question}
                answer={this.state.answer}
                getNext={this.getNext}
                getAnswer={this.getAnswer}
              />
            </CSSTransition>
          </TransitionGroup>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  questions: state.quizzes.quiz.questions
});

const mapDispatchToProps = dispatch => ({
  getResult: (score, history) => dispatch(startGetUserResult(score, history))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Quiz);
