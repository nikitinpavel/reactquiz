import React from 'react';
import {Link} from 'react-router-dom';

export default function Layout(props) {
  return (
    <>
      <header className="header">
        <h1 className="header__heading"><Link to="/">React Quiz</Link></h1>
      </header>
        <main className="content">
          {props.children}
        </main >
      <footer className="footer">
        <p className="footer__info">
          В приложении использована информация с сайта Meduza, здесь
          <a className="app-link" href="https://meduza.io/quiz/eto-lisichka-ili-poganka-proverte-kak-horosho-vy-razbiraetes-v-gribah">
            оригинальный тест
          </a>
        </p>
      </footer>
    </>
  )
}
