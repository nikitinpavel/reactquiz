import React from 'react'

const Question = (props) => {
  const { questionNumber, questionsCount, question, answer, getNext, getAnswer } = props;

  // returns string for option className according to user answer
  const getOptionClassName = (questionAnswer, userAnswer, option) => {
    if (userAnswer && userAnswer === questionAnswer && questionAnswer === option) {
      return "question__option question__option--answered question__option--right"
    } else if ( userAnswer && userAnswer === option && userAnswer !== questionAnswer) {
      return "question__option question__option--answered question__option--wrong"
    }  else if (userAnswer) {
      return "question__option question__option--answered"
    }
    return "question__option"
  }


  return (
    
      <div className="question">
            <p className="question__count">
              {questionNumber}/{questionsCount}
            </p>
            <h3 className="question__text">{question.question_text}</h3>
            <img className="question__img" src={question.question_image} alt="mashroom" />
            <div className="question__options-wrap">
              <label className={getOptionClassName(question.answer, answer, "1")} htmlFor="answer1">{question.question_option1}
              {answer === "1" ? (
                  <p className="question__comment">{question.question_comment1}</p>
              ) : null}
              </label>
              <input
                disabled={answer}
                name="answers"
                type="radio"
                checked={answer === "1"}
                hidden
                value="1"
                id="answer1"
                onChange={getAnswer}
              />
              <label className={getOptionClassName(question.answer, answer, "2")} htmlFor="answer2">{question.question_option2}
                {answer === "2" ? (
                  <p className="question__comment">{question.question_comment2}</p>
                ) : null}
              </label>
              <input
                disabled={answer}
                name="answers"
                type="radio"
                checked={answer === "2"}
                hidden
                value="2"
                id="answer2"
                onChange={getAnswer}
              />
            </div>

            {answer ? (
              <button className="app-button" onClick={getNext}>
                {questionNumber === questionsCount
                  ? "ваш результат"
                  : "дальше"}
              </button>
            ) : null}

          </div>
    
  )
}

export default Question
