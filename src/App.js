import React from 'react';
import { Routes } from './routes/Routes';
import { Provider } from 'react-redux';
import { configureStore } from './store/configureStore';

import { startGetQizzes } from './actions/quizzesActions';
import {getQuizData} from './actions/quizzesActions';


const store = configureStore();

const url = 'https://www.24verstka.ru/quiz/wp-json/quizzes/v1/quizzeslist/';
store.dispatch(startGetQizzes(url));

const quizData = JSON.parse(sessionStorage.getItem('quizData'));

if (quizData !== null) {
  store.dispatch(getQuizData(quizData));
}

const App = (props) => {
  return (
    <Provider store={store} >
      <Routes />
    </Provider>
  );
}

export default App;